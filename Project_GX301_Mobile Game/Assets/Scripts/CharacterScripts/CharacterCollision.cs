﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterCollision : MonoBehaviour
{
    private void OnCollisionEnter(Collision col)
    {

        DebrisParent debrisType = col.collider?.GetComponentInParent<DebrisParent>();

        if (debrisType != null)
        {

            GameManager.instance.PlayerCrashed(debrisType);
        }
        if (col.gameObject.CompareTag("Target"))
        {

            GameManager.instance.GameOver();
        }

        GetComponent<AudioSource>().Play();
    }
}
