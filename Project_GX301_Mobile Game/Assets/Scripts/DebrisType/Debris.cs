﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Debris : MonoBehaviour
{
    public delegate void NearMis();
    public static event NearMis OnNearMis;
    private void OnTriggerEnter(Collider nearMis)
    {
        if (nearMis.CompareTag("Player"))
        {
            OnNearMis();
        }


        if (OnNearMis != null)
        {
            return;
        }


    }
}
