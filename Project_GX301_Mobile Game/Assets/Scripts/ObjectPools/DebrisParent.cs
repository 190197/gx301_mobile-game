﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum DebrisType
{
    Meteor,
    Drone,
    SpaceShip,
    Wreckage,
    
}

[System.Serializable]
public struct DebrisConfig
{
    public DebrisType debrisTypeName;

    public DebrisConfig(DebrisType _debrisTypeName)
    {
        this.debrisTypeName = _debrisTypeName;
    }
}

public class DebrisParent : MonoBehaviour
{
    [SerializeField] DebrisConfig debrisConfig;

    public void SetDebrisPrefab(DebrisConfig _debrisConfig)
    {
        debrisConfig = _debrisConfig;

        switch (debrisConfig.debrisTypeName)
        {
            case DebrisType.Meteor:
                {
                    transform.GetChild(0).gameObject.SetActive(true);
                    transform.GetChild(1).gameObject.SetActive(false);
                    transform.GetChild(2).gameObject.SetActive(false);
                    transform.GetChild(3).gameObject.SetActive(false);
                    break;
                }
            case DebrisType.Drone:
                {

                    transform.GetChild(0).gameObject.SetActive(false);
                    transform.GetChild(1).gameObject.SetActive(true);
                    transform.GetChild(2).gameObject.SetActive(false);
                    transform.GetChild(3).gameObject.SetActive(false);
                    break;
                }
            case DebrisType.SpaceShip:
                {
                    transform.GetChild(0).gameObject.SetActive(false);
                    transform.GetChild(1).gameObject.SetActive(false);
                    transform.GetChild(2).gameObject.SetActive(true);
                    transform.GetChild(3).gameObject.SetActive(false);
                    break;
                }
            case DebrisType.Wreckage:
                {
                    transform.GetChild(0).gameObject.SetActive(false);
                    transform.GetChild(1).gameObject.SetActive(false);
                    transform.GetChild(2).gameObject.SetActive(false);
                    transform.GetChild(3).gameObject.SetActive(true);
                    break;
                }

            default:
                {
                    transform.gameObject.SetActive(true);
                    transform.GetChild(0).gameObject.SetActive(false);
                    transform.GetChild(1).gameObject.SetActive(false);
                    transform.GetChild(2).gameObject.SetActive(false);
                    transform.GetChild(3).gameObject.SetActive(false);
                    break;
                }
        }

    }


}
