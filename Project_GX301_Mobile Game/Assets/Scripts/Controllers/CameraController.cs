﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] Transform target;
    [SerializeField] float smoothSpeed = 0.125f;
    [SerializeField] float rotateSpeed = 0.95f;
    [SerializeField] Vector3 offset;
    [SerializeField] Vector3 velocity = Vector3.zero;
    Camera camera;
    private void Awake()
    {
        offset = transform.position - target.position;
        camera = gameObject.GetComponent<Camera>();


        if (target == null)  // If the variable is not set in the inspector
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
    }
    private void FixedUpdate()
    {
        CameraFollow();
    }


    void CameraFollow()
    {
        Vector3 desiredPosition = target.position + offset;
        Vector3 smoothedPosition = Vector3.SmoothDamp(transform.position, desiredPosition, ref velocity, smoothSpeed);



        transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(new Vector3(85f, 0f, 0f)), rotateSpeed * Time.deltaTime);

        transform.position = smoothedPosition;

        camera.farClipPlane = Mathf.Lerp(camera.farClipPlane, 1000, 1 * Time.deltaTime);


    }


}

