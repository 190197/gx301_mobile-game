﻿using UnityEngine;

public class CharacterController : MonoBehaviour
{
    [SerializeField] float speed = 20;
    [SerializeField] float upSpeed = 2f;
    [SerializeField] float maxSpeed = 100f;

    [SerializeField] Rigidbody rb;

    private void Start()
    {
        if (rb == null)
        {
            rb = GetComponent<Rigidbody>();
        }

        Input.gyro.enabled = true;
    }

    private void Update()
    {
        if (Time.timeScale != 0)
        {
            Quaternion deviceRotation = DeviceRotation.Get();

            transform.rotation = deviceRotation ;
            rb.AddForce(transform.forward * speed, ForceMode.Acceleration);
            rb.velocity = Vector3.ClampMagnitude(rb.velocity, maxSpeed);
        }

    }
}
