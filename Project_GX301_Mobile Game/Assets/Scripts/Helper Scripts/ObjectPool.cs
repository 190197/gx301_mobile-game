﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool<T> : MonoBehaviour where T : MonoBehaviour
{
    [SerializeField] GameObject opPrefab;
    Queue<T> pool = new Queue<T>();

    public static ObjectPool<T> instance;

    private void Awake()
    {
        instance = this;
    }

    public T GetFromObjectPool()
    {
        T opObject = default(T);

        if (pool.Count > 0)
        {
            opObject = pool.Dequeue();
        }
        else
        {
            opObject = Instantiate(opPrefab, transform).GetComponent<T>();
        }
        opObject.gameObject.SetActive(true);
        return opObject;
    }

    public void PutIntoObjectPool(T opObject)
    {
        pool.Enqueue(opObject);

        opObject.transform.SetParent(transform, false);
        opObject.gameObject.SetActive(false);
    }
}
