﻿using UnityEngine;

public static class DeviceRotation
{
    private static bool gyroInitialised = false;

    public static bool HasGyroscope
    { // Sees if phone has a gyroscope
        get
        {
            return SystemInfo.supportsGyroscope;
        }
    }

    public static Quaternion Get()
    { // see if has initialised
        if (!gyroInitialised)
        {
            InitGyro();
        }
        return HasGyroscope
            ? ReadGyroscopeRotation()
            : Quaternion.identity;
    }

    private static void InitGyro()
    {
        if (HasGyroscope)
        {
            Input.gyro.enabled = true;              // enable gyroscope
            Input.gyro.updateInterval = 0.0167f;    // set the update interval to it's highest value (60Hz)
        }
        gyroInitialised = true;
    }

    private static Quaternion ReadGyroscopeRotation()
    {
        return new Quaternion(0.5f, 0.5f, -0.5f, 0.5f) * Input.gyro.attitude * new Quaternion(0, 0, 1, 2.6f); // matches the quaternion to match up with unity directions 2.6f To rotate character
    }
}
