﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetFrameRate : MonoBehaviour
{
    private void Start() {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 30;
    }
}
