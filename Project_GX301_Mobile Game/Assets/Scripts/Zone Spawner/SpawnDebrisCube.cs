﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnDebrisCube : SpawnZone
{
    [SerializeField] bool surfaceOnly;

    [SerializeField] float min = -0.5f;
    [SerializeField] float max = 0.5f;

    public override Vector3 SpawnPoint
    {
        get
        {
            Vector3 p;
            p.x = Random.Range(min, max);
            p.y = Random.Range(min, max);
            p.z = Random.Range(min, max);
            if (surfaceOnly)
            {
                int axis = Random.Range(0, 3);
                p[axis] = p[axis] < 0f ? min : max;
            }
            return transform.TransformPoint(p);
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.matrix = transform.localToWorldMatrix;
        Gizmos.DrawWireCube(Vector3.zero, Vector3.one);
    }


}
