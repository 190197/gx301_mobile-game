﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateZone : MonoBehaviour
{
    public static CreateZone instance; 
    [Header ("Refrence To Debris Zone")]
    public SpawnZone spawnZone;

    [Header ("The Types of Debris")]
    [SerializeField] DebrisConfig[] debrisConfig; // For types of objects that can be spawned

    
    [Header ("Amount of debris in Zone")]
    [SerializeField] float amountOfDebris;

   [SerializeField] List<DebrisParent> spawnedDebris = new List<DebrisParent>();
    private void Awake() {
        instance = this ;
        if(Time.timeScale == 0)
        StartCoroutine(FillZone());
    }



    void FillSpawnZone(){
        for (int i = 0; i < amountOfDebris; i++)
        {
            DebrisParent debrisParent = GetDebris();

            debrisParent.transform.localPosition = spawnZone.SpawnPoint;

            if(debrisParent != null){

                spawnedDebris.Add(debrisParent);
            }
        }
    }

    public DebrisParent GetDebris(){

        DebrisParent debrisParent = null;

        debrisParent = DebrisPool.instance.GetFromObjectPool();

        //debris.transform.parent = transform; // If weird stuff happens take this out. // Weird stuff did happen

        debrisParent.SetDebrisPrefab(debrisConfig[Random.Range(0,debrisConfig.Length)]);  // Selects a random opject from array of objects that can be spawned.
        
        return debrisParent;
    }

        IEnumerator FillZone()
    {
       yield return new WaitForSeconds(1);

       FillSpawnZone();
    }

}

