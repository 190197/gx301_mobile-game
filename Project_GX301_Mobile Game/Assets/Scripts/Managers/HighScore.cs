﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScore : MonoBehaviour
{
    public static HighScore instance;
    [SerializeField] Text highScoreDisplay;
    [SerializeField] Text multiplierDisplay;
    [SerializeField] GameObject player;
    [SerializeField] float scoreMultiplierMax = 8f;
    [SerializeField] Animator anim;
    Vector3 playerPostion;
    Vector3 lastPosition;
    float _highScore; // distance traveled
    float scoreMultiplier = 1f;

    private void Awake()
    {
        instance = this;
        multiplierDisplay.gameObject.SetActive(false);
        playerPostion = player.transform.position;
        lastPosition = playerPostion;

    }

    private void OnEnable()
    {
        Debris.OnNearMis += IncreaseMultiplier;
    }
    private void OnDisable()
    {
        Debris.OnNearMis += IncreaseMultiplier;
    }
    private void LateUpdate()
    {
        if (Time.timeScale == 1)
        {

            playerPostion = player.transform.position;
            _highScore += Mathf.Floor((Vector3.Distance(playerPostion, lastPosition) / 10) * scoreMultiplier);
            highScoreDisplay.text = _highScore.ToString();
            ShowMultiplier();


        }
    }
    void ShowMultiplier()
    {
        if (scoreMultiplier >= 2)
        {
            multiplierDisplay.gameObject.SetActive(true);
            if (Time.timeScale == 0)
            {
                multiplierDisplay.gameObject.SetActive(false);
            }
            else if (Time.timeScale == 1)
            {
                multiplierDisplay.gameObject.SetActive(true);
            }

        }
    }
    public float SetHighScore()
    {
        return _highScore;
    }

    public void IncreaseMultiplier()
    {

        if (scoreMultiplier < scoreMultiplierMax)
        {
            scoreMultiplier += 1f;
            if (multiplierDisplay == null) return;
            anim.SetTrigger("Juice");
            multiplierDisplay.text = "x " + scoreMultiplier.ToString();
        }
    }

    void multiplierJuice()
    {

        if (multiplierDisplay == null) return;
    }

}
