﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;
    [SerializeField] Canvas gameCountDown;

    [SerializeField] Canvas advertCanvas;
    [SerializeField] Canvas gameOverCanvas;
    [SerializeField] float timeToGameStart = 5f;
    [SerializeField] Text countDownTimer;
    [SerializeField] Text highScore;
    bool isGamePaused;
    DebrisParent collisionObject;


    private void Awake()
    {
        instance = this;
        isGamePaused = true;
        Time.timeScale = 0;

    }
    private void Start()
    {
        advertCanvas.gameObject.SetActive(false);
        gameOverCanvas.gameObject.SetActive(false);
        if (countDownTimer != null)
        {
            StartCoroutine("StartTimer");
        }

        else
        {
            Debug.LogWarning($"Count Down Timer has not been assigned to code.");
        }
    }

    IEnumerator StartTimer()
    {

        while (timeToGameStart > 0 && isGamePaused)
        {
            gameCountDown.gameObject.SetActive(true);
            timeToGameStart -= 1;
            countDownTimer.text = timeToGameStart.ToString();
            yield return new WaitForSecondsRealtime(1);

            if (timeToGameStart <= 0)
            {
                Time.timeScale = 1; //Mathf.Lerp(0, 1, 0.125f );
                isGamePaused = !isGamePaused;
                gameCountDown.gameObject.SetActive(false);
            }
        }
    }

    public void PlayerCrashed(DebrisParent _collisionObject)
    {
        collisionObject = _collisionObject;

        ResumePlay();

        advertCanvas.gameObject.SetActive(true);
    }

    public void WatchAd()
    {
        DebrisPool.instance.PutIntoObjectPool(collisionObject);
        advertCanvas.gameObject.SetActive(false);

        StartCoroutine("StartTimer");

    }

    public void GameOver(){
        Time.timeScale = 0;
        gameOverCanvas.gameObject.SetActive(true);

        highScore.text = HighScore.instance.SetHighScore().ToString();
    }

    public void ResumePlay()
    {
        Time.timeScale = 0;
        isGamePaused = true;
        timeToGameStart = 5f;
    }
}
