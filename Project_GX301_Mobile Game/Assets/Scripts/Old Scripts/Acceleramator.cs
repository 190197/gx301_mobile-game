﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Acceleramator : MonoBehaviour
{

    Vector2 dir = Vector2.zero;
    float speed = 75;
    Rigidbody rigidbody;
    [SerializeField] Text debug;
    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
        Application.targetFrameRate = 30; // Set this some where else
    }
    private void FixedUpdate()
    {
        CharacterMovement();
    }

    void CharacterMovement()
    {

        dir.x = Input.acceleration.x;
        dir.y = -Input.acceleration.y; // using vector 2 (Vector 3 extreme y slowdown and speeds up.)

        if (dir.sqrMagnitude > 1)
        {
            dir.Normalize();
        }

        debug.text = "X | " + dir.x.ToString() + " || " + "Y | " + dir.y.ToString() + " |";

        dir *= Time.deltaTime;
        transform.Translate(dir * speed);
        // rigidbody.AddForce (dir * speed);
    }

}